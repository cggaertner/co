CC := gcc
RM := rm -f
CLANG := clang

EXE := .exe
CFLAGS := -std=c99 -pedantic -Wall -Wextra -O3
CLANG_FLAGS := -std=c99 -Werror -Weverything -Wno-padded -Wno-missing-noreturn -Wno-undefined-internal

CHECK_SYNTAX = $(CLANG) -fsyntax-only $(CLANG_FLAGS) -I. $^
COMPILE = $(CC) -c $(CFLAGS) -I. -o $@ $<
BUILD = $(CC) $(CFLAGS) -I. -o $@ $^

HEADERS := co.h $(wildcard platform/*.h)
SOURCES := co.c $(wildcard examples/*.c)
EXAMPLES := $(patsubst examples/%.c,%$(EXE),$(filter examples/%,$(SOURCES)))

.PHONY : build clean examples syntax-check

build : co.o

examples : $(EXAMPLES)

clean :
	$(RM) co.o $(EXAMPLES)

syntax-check : $(SOURCES)
	$(CHECK_SYNTAX)

co.o : co.c $(HEADERS)
	$(COMPILE)

$(EXAMPLES) : %$(EXE) : examples/%.c co.o
	$(BUILD)
