/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef CO_H_
#define CO_H_

#include <stddef.h>

enum { CO_READY, CO_BLOCKING, CO_DEAD };
/*	internal coroutine states
	a ready coroutine can be resumed via co_call() or co_switch()
	a blocking coroutine waits for a callee to return and cannot be resumed
	you should never encounter a dead coroutine
*/

typedef struct co_routine *co_t;
/* a coroutine */

void co_init(void);
/*	initialize the coroutine environment
	right now, just determines the system's page size
*/

co_t co_active(void);
/*	return currently active coroutine
*/

int co_state(co_t co);
/*	return the state of the coroutine
	something's off if this is neither CO_READY nor CO_BLOCKING
*/

co_t co_create(void entry(void *), const void *arg, size_t page_count);
/*	create a coroutine and reserve page_count memory pages for its stack
	arg will be passed to entry() on first call
	returns NULL if allocation fails
*/

_Bool co_call(co_t co);
/*	call a newly created coroutine or resume a suspended one
	changes the calling coroutine's state to blocking
	returns 1 if the coroutine can be resumed, 0 if it has run its course
*/

co_t co_switch(co_t co);
/*	start a newly create coroutine or resume a suspended one
	in contrast to co_call(), does not block the caller
	returns the previously active coroutine
*/

void co_cancel(co_t co);
/*	cancel a suspended coroutine and discard reserved memory pages
	calls abort() if the coroutine is scheduled or if the memory pages cannot
	be discarded
*/

void co_yield(void);
/*	suspend the active coroutine and return to caller
	if the coroutine was never resumed via co_call() or it has yielded since,
	the behaviour is undefined
*/

void co_return(void);
/*	terminate the active coroutine and return to caller
	automatically discards reserved memory pages
	if the coroutine was never resumed via co_call() or it has yielded since,
	the behaviour is undefined
*/

void co_enqueue(co_t co);
/*	add coroutine to end of scheduler queue
	calls abort() is the coroutine is already scheduled
*/

void co_dequeue(void);
/*	remove active coroutine from scheduler queue
	calls abort() if the active coroutine is not the currently scheduled one
*/

void co_schedule(void);
/*	resume next non-blocking coroutine in scheduler queue
	if there's no such coroutine, the behaviour is undefined
*/

#endif
