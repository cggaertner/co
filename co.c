/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"

#define asmfn(NAME, BODY) \
	__asm__ (#NAME); \
	__asm__ ("\t.text\n" #NAME ":\n" BODY)

typedef void *ctx_t[1];

struct co_routine
{
	ctx_t ctx;
	void *pages;
	size_t size;
	void (*entry)(void *);
	void *arg;
	co_t caller;
	co_t next;
	int state;
};

void abort(void);

#if defined __amd64__
#include "platform/amd64.h"
#include "platform/down.h"
#elif defined __i386__
#include "platform/i386.h"
#include "platform/down.h"
#else
#error "unsupported architecture"
#endif

#if defined _WIN32
#include "platform/win32.h"
#elif defined __unix__
#include "platform/unix.h"
#else
#error "unsupported operating system"
#endif

static size_t page_size;

static struct co_routine root = { .next = &root };
static struct co_routine dummy = { .state = CO_DEAD };
static co_t active = &root;
static co_t head = &root;
static co_t tail = &root;

static ctx_t nursery;
static co_t baby;

static void __attribute__ ((__noinline__, __regparm__(2)))
switch_context(void **src, void **dest) asmfn(switch_context,
	STORE_CONTINUATION
	SWITCH_STACKS
	LOAD_CONTINUATION
	CALL_CONTINUATION
);

static void __attribute__ ((__noinline__, __regparm__(2)))
call_trampoline(void **src, void **dest) asmfn(call_trampoline,
	STORE_CONTINUATION
	SWITCH_STACKS
	CALL_TRAMPOLINE
);

static void __attribute__ ((__used__))
trampoline(void) __asm__ ("trampoline");

static void trampoline(void)
{
	co_t self = baby;
	switch_context(baby->ctx, nursery);
	active = self;

	active->entry(active->arg);
	abort();
}

void co_init(void)
{
	page_size = get_page_size();
}

co_t co_active(void)
{
	return active;
}

int co_state(co_t co)
{
	return co->state;
}

co_t co_create(void entry(void *), const void *arg, size_t page_count)
{
	size_t size = page_size * page_count;
	char *pages = alloc_pages(size);
	if(!pages) return NULL;

	void *lo = pages, *hi = pages + size;
	baby = get_co(lo, hi);
	baby->ctx[0] = get_stack(lo, hi);
	baby->pages = pages;
	baby->size = size;
	baby->entry = entry;
	baby->arg = (void *)arg;

	call_trampoline(nursery, baby->ctx);

	return baby;
}

void co_cancel(co_t co)
{
	if(co->next)
		abort();

	if(!free_pages(co->pages, co->size))
		abort();
}

_Bool co_call(co_t co)
{
	if(co->caller)
		abort();

	active->state = CO_BLOCKING;
	co->caller = active;

	co_switch(co);

	active->state = CO_READY;
	co->caller = NULL;

	if(co->state == CO_DEAD)
	{
		co_cancel(co);
		return 0;
	}

	return 1;
}

co_t co_switch(co_t co)
{
	if(co->state != CO_READY)
		abort();

	co_t self = active;
	switch_context(active->ctx, co->ctx);
	co_t prev = active;
	active = self;
	return prev;
}

void co_yield(void)
{
	if(active->caller->state != CO_BLOCKING)
		abort();

	co_t self = active;
	switch_context(active->ctx, active->caller->ctx);
	active = self;
}

void co_return(void)
{
	if(active->caller->state != CO_BLOCKING)
		abort();

	active->state = CO_DEAD;
	switch_context(active->ctx, active->caller->ctx);
	abort();
}

void co_enqueue(co_t co)
{
	if(co->next)
		abort();

	co->next = head;
	tail->next = co;
	tail = co;
}

void co_dequeue(void)
{
	if(active != head)
		abort();

	co_t next = active->next;
	active->next = NULL;

	dummy.next = next;
	tail->next = next;
	head = &dummy;
}

void co_schedule(void)
{
	do
	{
		tail = head;
		head = head->next;
	}
	while(head->state != CO_READY);

	co_switch(head);
}
