/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

enum
{
	MAX_ALIGN = 2 * sizeof (void *),
	ROUTINE_SIZE = sizeof (struct co_routine),
	STACK_OFFSET = ((ROUTINE_SIZE + MAX_ALIGN - 1) / MAX_ALIGN) * MAX_ALIGN
};

static inline co_t get_co(void *lo, void *hi)
{
	(void)lo;
	return (void *)((char *)hi - STACK_OFFSET);
}

static void *get_stack(void *lo, void *hi)
{
	(void)lo;
	return (void *)((char *)hi - STACK_OFFSET);
}
