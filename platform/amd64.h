/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#if defined _WIN32

#define STORE_CONTINUATION "\
	subq $168, %rsp\n\
	movaps %xmm6, (%rsp)\n\
	movaps %xmm7, 16(%rsp)\n\
	movaps %xmm8, 32(%rsp)\n\
	movaps %xmm9, 48(%rsp)\n\
	movaps %xmm10, 64(%rsp)\n\
	movaps %xmm11, 80(%rsp)\n\
	movaps %xmm12, 96(%rsp)\n\
	movaps %xmm13, 112(%rsp)\n\
	movaps %xmm14, 128(%rsp)\n\
	movaps %xmm15, 144(%rsp)\n\
	pushq %rsi\n\
	pushq %rdi\n\
	pushq %rbp\n\
	pushq %rbx\n\
	pushq %r12\n\
	pushq %r13\n\
	pushq %r14\n\
	pushq %r15\n\
"

#define LOAD_CONTINUATION "\
	popq %r15\n\
	popq %r14\n\
	popq %r13\n\
	popq %r12\n\
	popq %rbx\n\
	popq %rbp\n\
	popq %rdi\n\
	popq %rsi\n\
	movaps (%rsp), %xmm6\n\
	movaps 16(%rsp), %xmm7\n\
	movaps 32(%rsp), %xmm8\n\
	movaps 48(%rsp), %xmm9\n\
	movaps 64(%rsp), %xmm10\n\
	movaps 80(%rsp), %xmm11\n\
	movaps 96(%rsp), %xmm12\n\
	movaps 112(%rsp), %xmm13\n\
	movaps 128(%rsp), %xmm14\n\
	movaps 144(%rsp), %xmm15\n\
	addq $168, %rsp\n\
"

#define SWITCH_STACKS "\
	movq %rsp, (%rcx)\n\
	movq (%rdx), %rsp\n\
"

#else

#define STORE_CONTINUATION "\
	pushq %rbp\n\
	pushq %rbx\n\
	pushq %r12\n\
	pushq %r13\n\
	pushq %r14\n\
	pushq %r15\n\
"

#define LOAD_CONTINUATION "\
	popq %r15\n\
	popq %r14\n\
	popq %r13\n\
	popq %r12\n\
	popq %rbx\n\
	popq %rbp\n\
"

#define SWITCH_STACKS "\
	movq %rsp, (%rdi)\n\
	movq (%rsi), %rsp\n\
"

#endif

#define CALL_CONTINUATION "\
	popq %rcx\n\
	jmpq *%rcx\n\
"
#define CALL_TRAMPOLINE "\
	callq trampoline\n\
"
