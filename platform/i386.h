/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#define STORE_CONTINUATION "\
	pushl %ebp\n\
	pushl %ebx\n\
	pushl %esi\n\
	pushl %edi\n\
"

#define LOAD_CONTINUATION "\
	popl %edi\n\
	popl %esi\n\
	popl %ebx\n\
	popl %ebp\n\
"

#define SWITCH_STACKS "\
	movl %esp, (%eax)\n\
	movl (%edx), %esp\n\
"

#define CALL_CONTINUATION "\
	popl %ecx\n\
	jmpl *%ecx\n\
"

#define CALL_TRAMPOLINE "\
	calll trampoline\n\
"
