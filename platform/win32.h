/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include <windows.h>

static inline size_t get_page_size(void)
{
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	return info.dwPageSize;
}

static inline void *alloc_pages(size_t size)
{
	return VirtualAlloc(NULL, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
}

static inline _Bool free_pages(void *pages, size_t size)
{
	(void)size;
	return VirtualFree(pages, 0, MEM_RELEASE);
}
