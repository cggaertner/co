/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include <unistd.h>
#include <sys/mman.h>

static inline size_t get_page_size(void)
{
	return (size_t)sysconf(_SC_PAGESIZE);
}

static inline void *alloc_pages(size_t size)
{
	void *pages = mmap(NULL, size, PROT_READ | PROT_WRITE,
		MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

	return pages != MAP_FAILED ? pages : NULL;
}

static inline _Bool free_pages(void *pages, size_t size)
{
	return munmap(pages, size) == 0;
}
