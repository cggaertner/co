/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"
#include <stdio.h>

void abort(void);

enum
{
	INIT,
	PET,
	KILL
};

typedef struct obj *obj_t;
typedef void (*class_t)(obj_t);

struct obj
{
	co_t co;
	int msg;
	void *arg;
};

struct buildargs
{
	obj_t obj;
	class_t class;
	void *init;
};

static void build(void *p)
{
	struct buildargs *args = p;
	struct obj self = { co_active(), INIT, args->init };
	args->obj = &self;
	args->class(&self);
	abort();
}

static obj_t obj_create(class_t class, const void *arg)
{
	struct buildargs args = { NULL, class, (void *)arg };
	co_call(co_create(build, &args, 1));
	return args.obj;
}

static void obj_call(obj_t obj, int msg, const void *arg)
{
	obj->msg = msg;
	obj->arg = (void *)arg;
	co_call(obj->co);
}

static void Cat(obj_t self)
{
	const char *name = NULL;

	for(;; co_yield()) switch(self->msg)
	{
		case INIT:
		name = self->arg;
		break;

		case PET:
		printf("%s purrs\n", name);
		break;

		case KILL:
		co_return();

		default:
		abort();
	}
}

static void Dog(obj_t self)
{
	const char *name = NULL;

	for(;; co_yield()) switch(self->msg)
	{
		case INIT:
		name = self->arg;
		break;

		case PET:
		printf("%s wags tail\n", name);
		break;

		case KILL:
		co_return();

		default:
		abort();
	}
}

int main(void)
{
	co_init();

	obj_t cat = obj_create(Cat, "Lily");
	obj_t dog = obj_create(Dog, "Doc");

	obj_call(cat, PET, NULL);
	obj_call(dog, PET, NULL);

	obj_call(cat, KILL, NULL);
	obj_call(dog, KILL, NULL);
}
