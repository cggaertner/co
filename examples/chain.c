/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"
#include <stdio.h>

static void bar(void *p)
{
	(void)p;
	puts(__func__);

	int i = 5;
	while(i--)
	{
		printf("%i\n", i);
		co_yield();
	}

	co_return();
}

static void foo(void *p)
{
	(void)p;
	puts(__func__);
	co_t cobar = co_create(bar, NULL, 1);
	while(co_call(cobar))
	{
		puts("called bar");
		co_yield();
	}

	co_return();
}

int main(void)
{
	co_init();

	puts(__func__);
	co_t cofoo = co_create(foo, NULL, 1);
	while(co_call(cofoo))
		puts("called foo");
}
