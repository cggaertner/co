/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"
#include <stdio.h>
#include <windows.h>

static void f1(void *p)
{
	for(;;)
	{
		puts(__func__);
		Sleep(500);
		co_switch(*(co_t *)p);
	}
}

static void f2(void *p)
{
	for(;;)
	{
		puts(__func__);
		Sleep(500);
		co_switch(*(co_t *)p);
	}
}

int main(void)
{
	co_t c1, c2;
	co_init();
	c1 = co_create(f1, &c2, 1);
	c2 = co_create(f2, &c1, 1);
	co_call(c1);
}
