/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"
#include <stdio.h>

static void one_to_ten(void *p)
{
	int *i = p;
	for(*i = 1; *i <= 10; ++*i)
		co_yield();

	co_return();
}

static void even_numbers(void *p)
{
	int *i = p;
	for(*i = 2;; *i += 2)
		co_yield();
}

int main(void)
{
	co_init();

	int count, value;
	co_t counter = co_create(one_to_ten, &count, 1);
	co_t generator = co_create(even_numbers, &value, 1);

	while(co_call(counter))
	{
		co_call(generator);
		printf("%i -> %i\n", count, value);
	}

	co_cancel(generator);
}
