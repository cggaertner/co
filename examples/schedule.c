/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"
#include <stdio.h>
#include <windows.h>

static void dump(void *p)
{
	for(;;)
	{
		puts(p);
		Sleep(500);
		co_schedule();
	}
}

int main(void)
{
	co_init();

	const char *strings[] = { "foo", "bar", "baz" };
	for(unsigned i = 0; i < sizeof strings / sizeof *strings; ++i)
		co_enqueue(co_create(dump, strings[i], 1));

	co_dequeue();
	co_schedule();
}
