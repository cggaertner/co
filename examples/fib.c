/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"
#include <stdio.h>

static void fib(void *p)
{
	int *i = p;
	int a = 0, b = 1;

	for(;;)
	{
		*i = a + b;
		a = b;
		b = *i;
		co_yield();
	}
}

int main(void)
{
	co_init();

	int value;
	co_t cofib = co_create(fib, &value, 1);

	int i = 10;
	while(i--)
	{
		co_call(cofib);
		printf("%i ", value);
	}

	co_cancel(cofib);
}
