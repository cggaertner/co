/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "co.h"
#include <stdio.h>

static const int COUNT = 1 << 16;

static void dump(int i)
{
	printf("\r%10i/%i", i, COUNT);
}

static void nl(void)
{
	puts("");
}

static void func(void *p)
{
	int i = *(int *)p + 1;
	dump(i);

	if(i < COUNT)
	{
		co_call(co_create(func, &i, 1));
		dump(i);
	}
	else nl();

	co_return();
}

int main(void)
{
	co_init();

	int i = 0;
	dump(i);
	co_call(co_create(func, &i, 1));
	dump(i);
	nl();
}
